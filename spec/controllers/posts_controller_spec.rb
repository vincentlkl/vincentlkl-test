require 'rails_helper'

RSpec.describe PostsController, type: :controller do

  let(:valid_attributes) {
    {
      :title => "new post title"
    }
  }

  context "for posts" do
    before(:each) do
      @post = FactoryGirl.create(:post)      
    end
    
    describe "GET index" do
      it "assigns all posts as @posts" do
        get :index        
        expect(assigns(:posts)).to include(@post)
      end
    end

    describe "GET new" do
      it "assigns a new post as @post" do
        post = Post.new
        get :new, {}
        expect(assigns(:post)).to be_a_new(Post)
      end
    end

    describe "POST create" do
      it "creates a new post" do
        post :create, {:post => valid_attributes}
        expect(assigns(:post)).to be_a(Post)
      end
    end

    describe "GET edit" do
      it "assigns the requested post as @post" do
        post = @post
        get :edit, {:id => post.to_param}
        expect(assigns(:post)).to eq(post)
      end
    end
    
    describe "PUT update" do
      describe "with valid params" do
        it "assigns the requested post as @post" do
          post = @post
          put :update, {:id => post.to_param, :post => valid_attributes}
          expect(assigns(:post).title).to eq("new post title")
        end
      end
    end

    describe "DELETE destroy" do
      it "destroy a post" do
        expect {
          delete :destroy, { id: @post.id}
        }.to change(Post, :count).by(-1)
      end
    end

  end

end
