VincentTest::Application.routes.draw do
  
  match "s3_direct_upload" => "posts#s3_direct_upload", :via => :post, as: "s3_direct_upload"

  resources :posts
  root 'posts#index'
end
