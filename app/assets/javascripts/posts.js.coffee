# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$ ->
  readURL = (input) ->
    if input.files and input.files[0]
      reader = new FileReader

      reader.onload = (e) ->
        $('#upload-image-preview').attr 'src', e.target.result
        return

      reader.readAsDataURL input.files[0]
    return

  $('#upload-image').change ->
    readURL this
    return

  $("#s3-uploader").S3Uploader
    progress_bar_target: $('.js-progress-bars')
    allow_multiple_files: false
  
  $('#s3-uploader').bind "s3_uploads_start", (e, content) ->
    $(".js-progress-bars").show()

  $('#s3-uploader').bind "s3_upload_complete", (e, content) ->
    $("#attachment_direct_upload_url").val(content["url"])
    $("#attachment_direct_upload_link").show()
    $("#attachment_direct_upload_link").attr("href", content["url"])

  $('#s3-uploader').bind "s3_upload_failed", (e, content) ->
    alert("#{content.filename} failed to upload max file upload is 1mb.")