# == Schema Information
#
# Table name: posts
#
#  id         :integer          not null, primary key
#  title      :string(255)
#  content    :text
#  created_at :datetime
#  updated_at :datetime
#

class Post < ActiveRecord::Base

  has_many :attachments, as: :attachable
  has_many :pictures, as: :imageable

  accepts_nested_attributes_for :pictures, :allow_destroy => true
  accepts_nested_attributes_for :attachments, :allow_destroy => true

  def self.strong_parameters
    columns = [:id, :title, :content, :pictures_attributes => [:id, :image], :attachments_attributes => [:id, :direct_upload_url]]
  end

end
