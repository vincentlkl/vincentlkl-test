# == Schema Information
#
# Table name: pictures
#
#  id                 :integer          not null, primary key
#  imageable_id       :integer
#  imageable_type     :string(255)
#  image_file_name    :string(255)
#  image_content_type :string(255)
#  image_file_size    :integer
#  image_updated_at   :datetime
#  created_at         :datetime
#  updated_at         :datetime
#

class Picture < ActiveRecord::Base
  belongs_to :imageable, :polymorphic => true

  has_attached_file :image, :styles => {
                            :small => "348X216" },
                            :default_url => "/assets/missing.png",
                            :storage => :s3,
                            :s3_host_name => "s3-#{ENV['S3_REGION']}.amazonaws.com",
                            :s3_region => ENV['S3_REGION'],
                            :s3_credentials => {:access_key_id => ENV['AWS_ACCESS_KEY_ID'],
                                                :secret_access_key => ENV['AWS_SECRET_ACCESS_KEY'],
                                                :bucket => ENV['S3_BUCKET_NAME'],
                                                }

  validates_attachment  :image,
        :presence => true,
        :content_type => { :content_type => ["image/jpeg", "image/gif", "image/png"] },
        :size => { :less_than => 5.megabyte }

end
