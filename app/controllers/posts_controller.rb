class PostsController < ApplicationController
  def index
    @posts = Post.all.order("created_at desc")
  end

  def show
    @post = Post.find(params[:id])
  end

  def new
    @post = Post.new
    @post.pictures.build
    @post.attachments.build
  end

  def edit
    @post = Post.find(params[:id])
    if @post.pictures.count == 0
      @post.pictures.build
    end

    if @post.attachments.count == 0
      @post.attachments.build
    end
  end

  def create
    @post = Post.new(post_params)
    if @post.save
      flash["notice"] = "Post has been created."
      redirect_to posts_path 
    else
      flash["alert"] = "Post has not been created."
      render "new"       
    end
  end

  def update
    @post = Post.find(params[:id])
    if (@post.update_attributes(post_params))
      flash["notice"] = "Post has been updated."
      redirect_to posts_path 
    else
      flash["alert"] = "Post has not been updated."
      render "edit" 
    end
  end

  def destroy
    @post = Post.find(params[:id])
    @post.destroy
    flash["notice"] = "Post has been deleted."
    redirect_to posts_path 
  end

  def s3_direct_upload
    render json: {status: :ok}
  end

private
    def post_params
      columns = Post.strong_parameters
      params.require(:post)
      .permit(columns)
    end

end
