class CreateAttachments < ActiveRecord::Migration
  def change
    create_table :attachments do |t|
      t.references :attachable, :polymorphic => true
      t.attachment :file
      t.string :direct_upload_url
      t.timestamps
    end
  end
end
